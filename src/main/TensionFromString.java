package main;

import com.sun.org.apache.xpath.internal.operations.Bool;
import featureX.Cli;
import featureX.featureY;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

public class TensionFromString {
    public static void main(String[] args) throws IOException {

//        System.out.println("-----------------");
//        System.out.println("MorpheuS Audio Tension Visualiser v2.0");
//        System.out.println("-----------------");
//        System.out.println("Calculating tension from input file. ");




        Cli inputarguments = new Cli(args);
        inputarguments.parse();
//        String file ="A B \nA B";
        String file = inputarguments.inputfile;
        file = file.replaceAll(", ", " \n");
//        System.out.print(file);
        Boolean externalfile = false;

        featureY pitchmodel = new featureY(file, externalfile);
//        pitchmodel.setMeterUnits(inputarguments.meterUnits);

        // calculate cloud diameter
        ArrayList<Double> diameters = new ArrayList<>();


        //todo detect key? yes or no



        //calculate tension profile
        //todo check detectKey in subfunctions
        Boolean detectKey = inputarguments.detectKey;

        pitchmodel.calculateTensionReduced(inputarguments.key, inputarguments.keymajor, detectKey);



        //todo set normalisation not for 0 off or on for longer or short fragments

//        System.out.println("Done. ");

    }



}
